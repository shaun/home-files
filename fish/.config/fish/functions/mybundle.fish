function mybundle
  if test -f Gemfile.private
    set -lx BUNDLE_GEMFILE Gemfile.private
    bundle $argv
  end
end

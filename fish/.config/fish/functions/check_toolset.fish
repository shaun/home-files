function check_toolset --description 'Check which tools are installed'
  function _check_commands
    set cmds asdf bat ctags fd fzf git node rg ruby rustup stow tmux tmuxinator vim

    for cmd in $cmds
      if command --search --quiet $cmd
        set_color a1b56c normal
        echo "✔ $cmd"
      else
        set_color ab4642 magenta
        echo "✖ $cmd"
      end
    end
  end

  function _check_functions
    set funcs omf

    for func in $funcs
      if functions --query $func
        set_color a1b56c normal
        echo "✔ $func"
      else
        set_color ab4642 magenta
        echo "✖ $func"
      end
    end
  end

  _check_functions
  _check_commands
end

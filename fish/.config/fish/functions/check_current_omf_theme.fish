function check_current_omf_theme --description 'Check the current OMF theme'
  set theme_file $HOME/.config/omf/theme

  if test -f $theme_file
    echo "The current Oh My Fish theme is:" (cat $theme_file)
  else
    echo "No themes are currently installed. First, you need to install Oh My Fish."
  end
end

vim.keymap.set(
  "n",
  "<Leader>xl",
  ":.lua<CR>",
  { buffer = true, noremap = true, silent = true, desc = "Execute current (l)ine" }
)

vim.keymap.set(
  "v",
  "<Leader>xl",
  ":lua<CR>",
  { buffer = true, noremap = true, silent = true, desc = "Execute selected (l)ines" }
)

vim.keymap.set("n", "<Leader>xf", function()
  vim.cmd("source %")
  vim.notify("Successfully reloaded '" .. vim.fn.expand("%:t") .. "'", "info")
end, { buffer = true, noremap = true, silent = true, desc = "Reload current (f)ile" })

require("which-key").add({
  { "<leader>x", group = "E(x)ecute" },
})

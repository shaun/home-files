return {
  -----------------------------------------------------------------------------
  -- nvim-treesitter
  -----------------------------------------------------------------------------
  {
    "https://github.com/nvim-treesitter/nvim-treesitter.git",
    build = ":TSUpdate",

    dependencies = {
      -- Automatically add end/closing tags (for Ruby, Lua, Elixir...)
      "https://github.com/RRethy/nvim-treesitter-endwise.git",
      -- Automatically add end/closing tags (for HTML, Js, Vue...)
      "https://github.com/windwp/nvim-ts-autotag.git",
    },

    -- These settings will get merged with LazyVim's settings
    opts = {
      ensure_installed = {
        "bash",
        "css",
        "diff",
        "eex",
        "elixir",
        "erlang",
        "fish",
        "go",
        "html",
        "heex",
        "javascript",
        "json",
        "lua",
        "luadoc",
        "ruby",
        "rust",
        "scss",
        "toml",
        "typescript",
        "vim",
        "vue",
        "vimdoc",
        "yaml",
      },

      auto_install = true, -- Automatically install missing parsers when entering buffer
      sync_install = false,

      highlight = {
        enable = true,
      },

      autotag = {
        enable = true,
      },

      endwise = {
        enable = true,
      },
    },
  },

  -----------------------------------------------------------------------------
  -- nvim-ts-autotag - autoclose and autorename html tags
  -----------------------------------------------------------------------------
  {
    "https://github.com/windwp/nvim-ts-autotag",
    opts = {
      aliases = {
        ["heex"] = "html",
      },
    },
  },

  -----------------------------------------------------------------------------
  -- nvim-treesitter-context - sticky headers when scrolling
  -----------------------------------------------------------------------------
  {
    "https://github.com/nvim-treesitter/nvim-treesitter-context.git",
    opts = {
      max_lines = 3, -- How many sticky lines to display
    },
  },
}

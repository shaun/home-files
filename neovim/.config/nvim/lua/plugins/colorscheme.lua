return {
  {
    "https://github.com/olimorris/onedarkpro.nvim.git",

    lazy = false,
    priority = 1000, -- Increase priority so colorscheme will get loaded first, default priority is 50
    opts = function()
      local color = require("onedarkpro.helpers")

      local tokyonight_colors = {
        moon_bg = "#222436",
        moon_highlight_bg = "#2f334d",
        moon_bright_highlight_bg = color.lighten("#2f334d", 18),
        moon_fg = "#c8d3f5",
        moon_comment = "#7a88cf",

        comment = "#565f89",
      }

      return {
        theme = "onedark_vivid",
        options = {
          cursorline = true,
        },
        colors = {
          -- Defining new colors
          bright_white = color.lighten("white", 15, "onedark_vivid"),
          dark_purple = color.darken("purple", 20, "onedark_vivid"),

          -- Overriding colors
          bg = tokyonight_colors.moon_bg,
          fg = tokyonight_colors.moon_fg,
          fg_gutter = tokyonight_colors.comment,

          cursorline = color.lighten(tokyonight_colors.moon_highlight_bg, 3),
          bg_statusline = tokyonight_colors.moon_highlight_bg,

          comment = tokyonight_colors.moon_comment,
        },
        styles = {
          keywords = "bold,italic",
          functions = "bold,italic",
          virtual_text = "italic",
          comments = "italic",
        },
        highlights = {
          -- LSP
          ["@property"] = { fg = "${yellow}", italic = true },

          -- Active line number
          CursorLineNr = { fg = "${yellow}" },

          -- Visual mode
          Visual = { bg = tokyonight_colors.moon_bright_highlight_bg },

          -- Cursor lines
          BlinkCmpMenuSelection = { bg = tokyonight_colors.moon_bright_highlight_bg },

          -- Searching for the word under the cursor
          CurSearch = {
            fg = "${bright_white}",
            bg = "${dark_purple}",
            bold = true,
            extend = true,
          },
          Search = {
            fg = "${bright_white}",
            bg = "${dark_purple}",
            bold = true,
            extend = true,
          },

          -- TreesitterContext (sticky headers when scrolling)
          TreesitterContext = { bg = color.lighten(tokyonight_colors.moon_highlight_bg, 3) },

          -- Borders
          BlinkCmpDocBorder = { fg = "${gray}" },
          BlinkCmpMenuBorder = { fg = "${gray}" },
          LspInfoBorder = { fg = "${gray}" },
          LspFloatWinBorder = { fg = "${gray}" },

          -- Floating windows that should standout more
          FloatBorderHighlighted = { fg = "${cyan}" },
          FloatTitleHighlighted = { fg = "${cyan}", style = "italic,bold" },
          BqfPreviewBorder = { fg = "${cyan}", style = "italic,bold" },

          -- Bqf (Better Quickfix)
          BqfPreviewBufLabel = { fg = "${cyan}", style = "italic,bold" },

          -- Dashboard
          DashboardHeader = { fg = "${blue}" },
          DashboardFooter = { fg = "${white}" },
          DashboardIcon = { fg = "${white}", style = "bold" },
          DashboardDesc = { fg = "${cyan}", style = "italic" },
          DashboardKey = { fg = "${white}", style = "bold" },

          -- Fidget (loading spinner for when LSP is working)
          FidgetTitle = { fg = "${purple}", style = "italic,bold" },
          FidgetTask = { fg = "${comment}", style = "italic" },

          -- Git diff colors
          DiffAdded = { fg = "${green}" },
          DiffChanged = { fg = "${orange}" },
          DiffRemoved = { fg = "${red}" },

          -- GitSigns
          GitSignsAdd = { fg = "${green}" },
          GitSignsChange = { fg = "${orange}" },
          GitSignsDelete = { fg = "${red}" },
        },
      }
    end,
  },
}

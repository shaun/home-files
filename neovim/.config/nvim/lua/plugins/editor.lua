return {
  "https://github.com/tpope/vim-repeat.git", -- Adds functionality to repeat surround and unimpaired commands
  "https://github.com/tpope/vim-surround.git", -- Surround.vim is all about 'surroundings': parentheses, brackets, quotes, XML tags, and more.
  "https://github.com/tpope/vim-unimpaired.git", -- Provides several pairs of bracket [] and toggle shortcuts

  ------------------------------------------------------------------------------
  -- mini.bufremove - buffer enhancements
  ------------------------------------------------------------------------------
  {
    "https://github.com/echasnovski/mini.bufremove.git",

    -- Override keymappings set by LazyVim
    keys = function()
      return {
        {
          "td",
          function()
            local bd = require("mini.bufremove").delete
            if vim.bo.modified then
              local choice = vim.fn.confirm(("Save changes to %q?"):format(vim.fn.bufname()), "&Yes\n&No\n&Cancel")
              if choice == 1 then -- Yes
                vim.cmd.write()
                bd(0)
              elseif choice == 2 then -- No
                bd(0, true)
              end
            else
              bd(0)
            end
          end,
          desc = "Delete current buffer",
        },
      }
    end,
  },

  ------------------------------------------------------------------------------
  -- nvim-bqf - quickfix window enhancements
  ------------------------------------------------------------------------------
  {
    "https://github.com/kevinhwang91/nvim-bqf.git",

    opts = {
      auto_enable = true,
      preview = {
        auto_preview = true,
        border = { "┏", "━", "┓", "┃", "┛", "━", "┗", "┃" },
      },
    },
  },

  ------------------------------------------------------------------------------
  -- nvim-tree - file explorer
  ------------------------------------------------------------------------------
  {
    "https://github.com/nvim-tree/nvim-tree.lua.git",

    opts = {
      actions = {
        open_file = {
          quit_on_open = true,
        },
      },
      renderer = {
        icons = {
          show = {
            git = false,
          },
        },
      },
    },
  },

  --------------------------------------------------------------------------------------------
  -- undotree - a visual undo tree
  --------------------------------------------------------------------------------------------
  {
    "https://github.com/mbbill/undotree.git",

    init = function()
      vim.cmd([[
        let g:undotree_SetFocusWhenToggle = 1
        let g:undotree_WindowLayout       = 2
      ]])
    end,
  },

  --------------------------------------------------------------------------------------------
  -- vim-togglelist - toggle quickfix list
  --------------------------------------------------------------------------------------------
  {
    "https://github.com/milkypostman/vim-togglelist.git",

    init = function()
      vim.g.toggle_list_no_mappings = false
    end,
  },

  --------------------------------------------------------------------------------------------
  -- which-key - displays a popup with possible key bindings of the command you started typing
  --------------------------------------------------------------------------------------------
  {
    "https://github.com/folke/which-key.nvim.git",

    -- Merge settings with LazyVim
    opts = {
      preset = "modern",
      registers = false, -- Don't display yanked registers
      icons = {
        group = "", -- Disable group icons
        mappings = false, -- Disable mapping icons
      },
      spec = {
        { "<leader>b", hidden = true }, -- Hide buffer menu item
        { "<leader>x", hidden = true }, -- Hide diagnostics/quickfix menu item
      },
      win = {
        border = { "┌", "─", "┐", "│", "┘", "─", "└", "│" },
      },
    },
  },

  -----------------------------------------------------------------------------
  -- yanky - yank-ring history picker
  -----------------------------------------------------------------------------
  {
    "https://github.com/gbprod/yanky.nvim.git",

    opts = function()
      require("telescope").load_extension("yank_history")

      local mapping = require("yanky.telescope.mapping")

      return {
        ring = {
          history_length = 500,
        },
        picker = {
          telescope = {
            mappings = {
              default = mapping.put("p"),
              i = {
                ["<C-d>"] = mapping.delete(),
              },
              n = {
                p = mapping.put("p"),
                P = mapping.put("P"),
                ["<C-d>"] = mapping.delete(),
              },
            },
          },
        },
      }
    end,
  },
}

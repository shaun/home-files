return {
  "https://github.com/tpope/vim-fugitive.git",
  "https://github.com/junegunn/gv.vim.git", -- Browse git commits

  -----------------------------------------------------------------------------
  -- git-messenger - reveals the commit messages under the cursor
  -----------------------------------------------------------------------------
  {
    "https://github.com/rhysd/git-messenger.vim.git",

    init = function()
      vim.cmd([[
        let g:git_messenger_always_into_popup   = v:true " Automatically move cursor into popup
        let g:git_messenger_include_diff        = "current"
        let g:git_messenger_max_popup_height    = 25
        let g:git_messenger_no_default_mappings = v:true
        let g:git_messenger_floating_win_opts   = { 'border': 'single' }
      ]])
    end,
  },

  -----------------------------------------------------------------------------
  -- gitsigns
  -----------------------------------------------------------------------------
  {
    "https://github.com/lewis6991/gitsigns.nvim.git",

    -- Override LazyVim settings for this plugin
    opts = function()
      return {
        signs = {
          add = { text = "▍" },
          change = { text = "▍" },
          untracked = { text = "▍" },
          topdelete = { text = "" },
          delete = { text = "" },
          changedelete = { text = "" },
        },
      }
    end,

    --  Disable LazyVim keymappings for this plugin
    keys = false,
  },

  -----------------------------------------------------------------------------
  -- Neogit
  -----------------------------------------------------------------------------
  {
    "https://github.com/NeogitOrg/neogit.git",

    dependencies = {
      "nvim-lua/plenary.nvim",
      "sindrets/diffview.nvim",
      "ibhagwan/fzf-lua",
    },

    config = true,
  },
}

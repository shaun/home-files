return {
  "https://github.com/jayp0521/mason-null-ls.nvim.git", -- Mason and null-ls integration
  "https://github.com/lukas-reineke/lsp-format.nvim.git", -- Auto-format on save functionality
  "https://github.com/onsails/lspkind.nvim.git", -- Adds pictogram icons to LSP

  -----------------------------------------------------------------------------
  -- mason-lspconfig - Mason and Neovim lspconfig integration
  -----------------------------------------------------------------------------
  {
    "https://github.com/williamboman/mason-lspconfig.nvim.git",

    opts = {
      automatic_installation = true, -- Automatically install LSP servers that are setup via nvim-lspconfig
    },
  },

  --------------------------------------------------------------------------------
  -- none-ls - Null language server (allows non-LSP sources to integrate with LSP)
  --------------------------------------------------------------------------------
  {
    "https://github.com/nvimtools/none-ls.nvim.git",

    init = function()
      local lsp_format = require("lsp-format")
      local mason_null_ls = require("mason-null-ls")
      local null_ls = require("null-ls")

      lsp_format.setup({}) -- Sets up auto-formatting when saving a file

      local sources = {
        -- diagnostics
        null_ls.builtins.diagnostics.trail_space.with({
          filetypes = { "*" },
          disabled_filetypes = { "lua", "NvimTree" },
        }),
        null_ls.builtins.diagnostics.gitlint.with({
          filetypes = { "gitcommit" },
          extra_args = { "--contrib=contrib-title-conventional-commits" },
        }),
      }

      local on_attach = function(client)
        lsp_format.on_attach(client)
      end

      null_ls.setup({
        sources = sources,
        on_attach = on_attach,
      })

      mason_null_ls.setup({
        ensure_installed = {},
        automatic_installation = true, -- Automatically install sources that null_ls sets up
      })
    end,
  },

  -----------------------------------------------------------------------------
  -- nvim-lspconfig
  -----------------------------------------------------------------------------
  {
    "https://github.com/neovim/nvim-lspconfig.git",
    dependencies = { "saghen/blink.cmp" },

    init = function()
      local capabilities = require("blink.cmp").get_lsp_capabilities()
      local keys = require("lazyvim.plugins.lsp.keymaps").get()
      local rust_tools = require("rust-tools")

      -- Disable LazyVim keymappings
      keys[#keys + 1] = { "gd", false, mode = "n" }
      keys[#keys + 1] = { "gD", false, mode = "n" }
      keys[#keys + 1] = { "gI", false, mode = "n" }
      keys[#keys + 1] = { "gk", false, mode = "n" }
      keys[#keys + 1] = { "gr", false, mode = "n" }
      keys[#keys + 1] = { "gy", false, mode = "n" }
      keys[#keys + 1] = { "K", false, mode = "n" }
      keys[#keys + 1] = { "<C-k>", false, mode = "i" }
      keys[#keys + 1] = { "<Leader>ca", false, mode = { "n", "v" } }
      keys[#keys + 1] = { "<Leader>cr", false, mode = { "n", "v" } }

      local on_attach = function(_, bufnr)
        local bufopts = { noremap = true, silent = true, buffer = bufnr }

        -- stylua: ignore
        vim.keymap.set(
          'n',
          'gd',
          function()
            require('telescope.builtin').lsp_definitions({ reuse_win = true }) end,
          vim.tbl_extend('force', bufopts, { desc = 'LSP -> Go to definition (of symbol under cursor)' })
        )
        vim.keymap.set(
          "n",
          "gD",
          vim.lsp.buf.declaration,
          vim.tbl_extend("force", bufopts, { desc = "LSP -> Go to declartion (of symbol under cursor)" })
        )
        -- stylua: ignore
        vim.keymap.set(
          'n',
          'gi',
          function() require("telescope.builtin").lsp_implementations({ reuse_win = true }) end,
          vim.tbl_extend('force', bufopts, { desc = 'LSP -> List all implementations (of symbol under cursor)' })
        )
        -- stylua: ignore
        vim.keymap.set(
          'n',
          'gT',
           function() require("telescope.builtin").lsp_type_definitions({ reuse_win = true }) end,
          vim.tbl_extend('force', bufopts, { desc = 'LSP -> Go to type definition (of symbol under cursor)' })
        )
        vim.keymap.set(
          "n",
          "gr",
          "<cmd>Telescope lsp_references<CR>",
          vim.tbl_extend("force", bufopts, { desc = "LSP -> List all references (of symbol under cursor)" })
        )
        vim.keymap.set(
          "n",
          "K",
          vim.lsp.buf.hover,
          vim.tbl_extend("force", bufopts, { desc = "LSP -> Display hover information (of symbol under cursor)" })
        )
        vim.keymap.set(
          "n",
          "<Leader>ca",
          vim.lsp.buf.code_action,
          vim.tbl_extend("force", bufopts, { desc = "LSP -> Show code actions" })
        )
        vim.keymap.set(
          "n",
          "<Leader>cd",
          vim.diagnostic.open_float,
          vim.tbl_extend("force", bufopts, { desc = "LSP -> Show all code diagnostics" })
        )
        vim.keymap.set("n", "<Leader>cf", function()
          vim.lsp.buf.format({ async = true })
        end, vim.tbl_extend("force", bufopts, { desc = "LSP -> Format code" }))
        vim.keymap.set(
          "n",
          "<Leader>ck",
          vim.lsp.buf.signature_help,
          vim.tbl_extend("force", bufopts, { desc = "LSP -> Display help/signature info (for symbol under cursor" })
        )
        vim.keymap.set(
          "n",
          "<Leader>cr",
          vim.lsp.buf.rename,
          vim.tbl_extend("force", bufopts, { desc = "LSP -> Rename all references (of symbol under the cursor)" })
        )
      end

      -- Elixir
      require("lspconfig").elixirls.setup({
        capabilities = capabilities,
        on_attach = on_attach,
      })

      -- Go
      require("lspconfig").gopls.setup({
        capabilities = capabilities,
        on_attach = on_attach,
      })

      -- JavaScript/TypeScript
      require("lspconfig").ts_ls.setup({
        capabilities = capabilities,
        on_attach = on_attach,
      })

      -- Lua
      require("lspconfig").lua_ls.setup({
        capabilities = capabilities,
        on_attach = on_attach,
        settings = {
          Lua = {
            runtime = {
              version = "LuaJIT",
            },
            diagnostics = {
              globals = { "vim" }, -- Get the language server to recognize the `vim` global
            },
            workspace = {
              library = vim.api.nvim_get_runtime_file("", true), -- Make the server aware of Neovim runtime files
              checkThirdParty = false,
            },
            telemetry = {
              enable = false,
            },
          },
        },
      })

      -- Ruby
      require("lspconfig").solargraph.setup({
        capabilities = capabilities,
        on_attach = on_attach,
        init_options = {
          formatting = false,
        },
        settings = {
          solargraph = {
            diagnostics = false,
          },
        },
      })

      -- Ruby
      require("lspconfig").ruby_lsp.setup({
        capabilities = capabilities,
        on_attach = on_attach,
      })

      -- Tailwind CSS
      require("lspconfig").tailwindcss.setup({
        capabilities = capabilities,
        on_attach = on_attach,
        settings = {
          tailwindCSS = {
            includeLanguages = {
              elixir = "html-eex",
              eelixir = "html-eex",
              heex = "html-eex",
            },
          },
        },
      })

      -- Vue.js
      require("lspconfig").volar.setup({
        capabilities = capabilities,
        on_attach = on_attach,
      })

      rust_tools.setup({
        server = {
          on_attach = on_attach,
          capabilities = capabilities,
        },
      })

      -- Add a border to the :LspInfo window
      require("lspconfig.ui.windows").default_options.border = "single"

      vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
        border = "single",
      })

      vim.diagnostic.config({ float = { border = "single" } })
    end,
  },
}

return {
  -----------------------------------------------------------------------------
  -- colorizer - color swatches
  -----------------------------------------------------------------------------
  {
    "https://github.com/NvChad/nvim-colorizer.lua.git",
    opts = {
      user_default_options = {
        mode = "virtualtext",
        virtualtext = "■■■",
        names = false, -- Don't show color swatches for "named" colors
        tailwind = true,
      },
    },
  },

  -----------------------------------------------------------------------------
  -- dressing - improves vim.ui.select & vim.ui.input interfaces
  -----------------------------------------------------------------------------
  {
    "https://github.com/stevearc/dressing.nvim.git",

    opts = {
      input = {
        relative = "editor", -- Position the input prompt in the center of the editor
        min_width = { 80, 0.5 }, -- Minimum of 80 columns/characters or 50% of the screen
        border = "single",
        win_options = {
          winblend = 0, -- 0% transparency
          winhighlight = "FloatBorder:FloatBorderHighlighted,FloatTitle:FloatTitleHighlighted",
        },
      },
      select = {
        telescope = {
          borderchars = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
        },
      },
    },
  },

  -----------------------------------------------------------------------------
  -- fidget - a loading spinner for when LSP is working
  -----------------------------------------------------------------------------
  {
    "https://github.com/j-hui/fidget.nvim.git",

    opts = {
      progress = {
        display = {
          progress_icon = { pattern = "dots_negative" },
        },
      },

      notification = {
        window = {
          winblend = 0,
        },
      },
    },
  },

  -----------------------------------------------------------------------------
  -- Indentation guidelines
  -----------------------------------------------------------------------------
  {
    "https://github.com/lukas-reineke/indent-blankline.nvim.git",

    -- These settings will get merged with LazyVim's settings
    opts = {
      indent = {
        char = "┊",
        highlight = {
          "TSRainbowCyan",
          "TSRainbowGreen",
          "TSRainbowViolet",
          "TSRainbowYellow",
        },
      },
      exclude = {
        filetypes = require("global-vars").filetypes_without_indent_guidelines,
      },
      scope = {
        enabled = false,
      },
    },
  },

  -----------------------------------------------------------------------------
  -- Disable Snack indentation guidelines
  -----------------------------------------------------------------------------
  {
    "https://github.com/folke/snacks.nvim",

    opts = {
      indent = {
        enabled = false,
      },
    },
  },

  -----------------------------------------------------------------------------
  -- lualine
  -----------------------------------------------------------------------------
  {
    "https://github.com/nvim-lualine/lualine.nvim.git",

    -- Override LazyVim settings
    opts = function()
      local onedarkpro = require("plugins/lualine/themes/onedarkpro")

      return {
        options = {
          theme = onedarkpro,
          globalstatus = true,
          ignore_focus = { "NvimTree" },
          component_separators = "",
          section_separators = "",
          padding = { left = 2, right = 2 },
        },

        sections = {
          lualine_a = {
            {
              "mode",
              fmt = function(str)
                return str:sub(1, 1)
              end,
              padding = {
                left = 1,
                right = 1,
              },
            },
          },
          lualine_b = {
            { "branch" },
            {
              "diagnostics",
              sources = { "nvim_diagnostic" },
              sections = { "error", "warn" },
              symbols = { error = "🔥", warn = "🔔" },
              colored = false,
            },
          },
          lualine_c = {
            {
              "filename",
              path = 1,
            },
          },
          lualine_x = {
            {
              "encoding",
              padding = {
                left = 1,
                right = 1,
              },
            },
            {
              "filetype",
              padding = {
                left = 1,
                right = 2,
              },
            },
          },
          lualine_y = {
            {
              "progress",
              padding = {
                left = 1,
                right = 1,
              },
            },
            {
              "location",
              padding = {
                left = 1,
                right = 1,
              },
            },
          },
          lualine_z = {},
        },
      }
    end,
  },

  -----------------------------------------------------------------------------
  -- Snacks dashboard
  -----------------------------------------------------------------------------
  {
    "https://github.com/folke/snacks.nvim",

    opts = {
      dashboard = {
        preset = {
          header = [[


███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗
████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║
██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║
██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║
██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║
╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝
          ]],

          -- stylua: ignore
          ---@type snacks.dashboard.Item[]
          keys = {
            { icon = " ", key = "e", desc = "File Explorer", action = ":NvimTreeOpen" },
            { icon = " ", key = "f", desc = "Find File", action = ":lua Snacks.dashboard.pick('files')" },
            { icon = " ", key = "n", desc = "New File", action = ":ene | startinsert" },
            { icon = "󱞳 ", key = "r", desc = "Restore Last Session", action = ':lua require("persistence").load()' },
            { icon = "󱞴 ", key = "R", desc = "Restore Other Sessions", action = ":Telescope persisted" },
            { icon = "󰦬 ", key = "p", desc = "Neovim Plugin Manager", action = ":Lazy" },
            { icon = "󰣪 ", key = "t", desc = "External Tools Manager", action = ":Mason" },
            { icon = "󰜎 ", key = "q", desc = "Quit", action = ":qa" },
          },
        },
        sections = {
          { section = "header" },
          { section = "keys", gap = 1, padding = 1 },
          { section = "startup" },
        },
      },
    },
  },

  -----------------------------------------------------------------------------
  -- Snacks notifier
  -----------------------------------------------------------------------------
  {
    "https://github.com/folke/snacks.nvim",

    opts = {
      notifier = {
        enabled = true,
      },

      styles = {
        notification = {
          border = "single",
        },

        ["notification.history"] = {
          border = "single",
        },
      },
    },
  },
}

return {
  {
    "https://github.com/mfussenegger/nvim-lint.git",

    -- These settings will get merged with LazyVim's settings
    opts = {
      linters_by_ft = {
        ["*"] = { "codespell" },
        elixir = { "credo" },
        gitcommit = { "alex" },
        markdown = { "alex" },
        ruby = { "standardrb" },
      },
    },
  },
}

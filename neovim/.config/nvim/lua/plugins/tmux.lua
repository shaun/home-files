return {
  -----------------------------------------------------------------------------
  -- tmuxline - generates a statusline for tmux
  -----------------------------------------------------------------------------
  {
    "https://github.com/edkolev/tmuxline.vim.git",

    ft = { "vim", "lua" },
    init = function()
      vim.cmd([[
        " -----------------------------------------------
        "   One Dark with Yellow Jelly Beans
        " -----------------------------------------------

        let g:black      = { 'gui': '#282c34', 'cterm': '236' }
        let g:dark_gray  = { 'gui': '#2f334d', 'cterm': '237' }
        let g:light_gray = { 'gui': '#565f89', 'cterm': '243' }
        let g:white      = { 'gui': '#c8d3f5', 'cterm': '188' }

        let g:red        = { 'gui': '#e06c75', 'cterm': '168' }
        let g:green      = { 'gui': '#98c379', 'cterm': '114' }
        let g:yellow     = { 'gui': '#fad07a', 'cterm': '180' }
        let g:blue       = { 'gui': '#61afef', 'cterm': '75'  }
        let g:magenta    = { 'gui': '#c678dd', 'cterm': '176' }
        let g:cyan       = { 'gui': '#56b6c2', 'cterm': '73'  }

        let g:tmuxline_powerline_separators = 0
        let g:tmuxline_preset = {
              \ 'a'    : '(ﾉ◕ヮ◕)ﾉ*:◦°✧#S',
              \ 'b'    : '',
              \ 'c'    : '',
              \ 'win'  : ' #I #W',
              \ 'cwin' : '#W✧◦°ヽʕ·ᴥ·ʔﾉ°◦✧#W',
              \ 'x'    : '',
              \ 'y'    : '',
              \ 'z'    : ''
              \ }

        let g:tmuxline_theme = {
              \ 'a'    : [ g:dark_gray.gui, g:white.gui ],
              \ 'b'    : [ g:white.gui, g:light_gray.gui ],
              \ 'c'    : [ g:white.gui, g:black.gui ],
              \ 'win'  : [ g:white.gui, g:light_gray.gui ],
              \ 'cwin' : [ g:black.gui, g:blue.gui],
              \ 'x'    : [ g:white.gui, g:black.gui ],
              \ 'y'    : [ g:white.gui, g:light_gray.gui ],
              \ 'z'    : [ g:dark_gray.gui, g:white.gui ],
              \ 'bg'   : [ g:white.gui, g:dark_gray.gui ],
              \ }
      ]])
    end,
  },

  -----------------------------------------------------------------------------
  -- vim-tmux-navigator - interact with tmux from Neovim
  -----------------------------------------------------------------------------
  {
    "https://github.com/christoomey/vim-tmux-navigator.git",
    init = function()
      vim.cmd("let g:tmux_navigator_no_mappings = 1") -- Disable builtin keymappings

      -- Use custom keymappings
      vim.keymap.set("n", "<leader>0l", ":TmuxNavigateLeft<cr>", { noremap = true, silent = true })
      vim.keymap.set("n", "<leader>0d", ":TmuxNavigateDown<cr>", { noremap = true, silent = true })
      vim.keymap.set("n", "<leader>0u", ":TmuxNavigateUp<cr>", { noremap = true, silent = true })
      vim.keymap.set("n", "<leader>0r", ":TmuxNavigateRight<cr>", { noremap = true, silent = true })

      -- Hide keymappings from Whichkey
      require("which-key").add({
        { "<leader>0l", hidden = true },
        { "<leader>0d", hidden = true },
        { "<leader>0u", hidden = true },
        { "<leader>0r", hidden = true },
      })
    end,
  },
}

return {
  {
    "https://github.com/Saghen/blink.cmp.git",

    dependencies = {
      -- Emoji completion
      "https://github.com/moyiz/blink-emoji.nvim",
    },

    opts = {
      completion = {
        list = {
          selection = {
            auto_insert = true,
            preselect = true,
          },
        },
        menu = {
          border = "single",
        },
        documentation = {
          window = {
            border = "single",
          },
        },
      },

      keymap = {
        preset = "enter",
        ["<Tab>"] = { "select_next", "snippet_forward", "fallback" },
        ["<S-Tab>"] = { "select_prev", "snippet_backward", "fallback" },
        ["<C-f>"] = { "select_and_accept", "fallback" },
      },

      sources = {
        default = {
          "emoji",
        },

        providers = {
          buffer = {
            opts = {
              -- Completion source for all normal buffers that are open
              get_bufnrs = function()
                return vim.tbl_filter(function(bufnr)
                  return vim.bo[bufnr].buftype == ""
                end, vim.api.nvim_list_bufs())
              end,
            },
          },

          emoji = {
            module = "blink-emoji",
            name = "Emoji",
            opts = { insert = true }, -- Insert emoji (instead of its name)
            should_show_items = function()
              return vim.tbl_contains(
                -- Filetypes to enable emoji completion for
                { "gitcommit", "markdown" },
                vim.o.filetype
              )
            end,
          },
        },
      },
    },
  },
}

return {

  ------------------------------------------------------------------------------
  -- Snacks - picker
  ------------------------------------------------------------------------------
  {
    "https://github.com/folke/snacks.nvim",

    -- Merge settings with LazyVim
    opts = {
      picker = {
        enabled = true,
        layouts = {
          default = {
            layout = {
              box = "horizontal",
              width = 0.8,
              min_width = 120,
              height = 0.8,
              {
                box = "vertical",
                border = "single",
                title = "{title} {live} {flags}",
                { win = "input", height = 1, border = "bottom" },
                { win = "list", border = "none" },
              },
              { win = "preview", title = "{preview}", border = "single", width = 0.5 },
            },
          },
        },
        layout = "default",
        matcher = {
          frequency = true,
        },
      },
    },
  },

  ------------------------------------------------------------------------------
  -- telescope
  ------------------------------------------------------------------------------
  {
    "https://github.com/nvim-telescope/telescope.nvim.git",

    dependencies = { "nvim-lua/plenary.nvim" },
    opts = function()
      local telescope_actions = require("telescope.actions")

      return {
        defaults = {
          results_title = "",
          dynamic_preview_title = true,
          borderchars = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
          sorting_strategy = "ascending",
          layout_config = {
            horizontal = {
              prompt_position = "top",
            },
          },
          mappings = {
            n = {
              ["<C-n>"] = telescope_actions.move_selection_next,
              ["<C-p>"] = telescope_actions.move_selection_previous,
              ["<C-q>"] = telescope_actions.smart_send_to_qflist + telescope_actions.open_qflist,
            },
            i = {
              ["<C-f>"] = telescope_actions.to_fuzzy_refine, -- Fuzzy refine the live_grep search results to find a file within the search results
              ["<C-u>"] = false, -- Unmap C-u - so instead of it scrolling up, it will clear the prompt line
              ["<C-q>"] = telescope_actions.smart_send_to_qflist + telescope_actions.open_qflist,
            },
          },
        },
        pickers = {
          find_files = {
            prompt_title = "",
          },
          buffers = {
            theme = "ivy",
            previewer = false,
            sort_mru = true,
            initial_mode = "insert",
            layout_config = {
              height = 0.45,
            },
            mappings = {
              i = {
                ["<C-d>"] = telescope_actions.delete_buffer,
              },
              n = {
                ["d"] = telescope_actions.delete_buffer,
                ["<C-d>"] = telescope_actions.delete_buffer,
                ["<SPACE>"] = { telescope_actions.close, type = "action", opts = { remap = true } },
              },
            },
          },
        },
        extensions = {
          fzf = {
            fuzzy = true,
            override_generic_sorter = true,
            override_file_sorter = true,
            case_mode = "smart_case",
          },
        },
      }
    end,

    init = function()
      require("telescope").load_extension("fzf")
      require("telescope").load_extension("yank_history")
      require("telescope").load_extension("persisted")
    end,
  },

  --------------------------------------------------------------------------------------------
  -- telescope-fzf-native
  --------------------------------------------------------------------------------------------
  {
    "nvim-telescope/telescope-fzf-native.nvim",

    build = "make",
  },
}

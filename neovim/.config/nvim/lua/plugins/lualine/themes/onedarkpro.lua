local colors = require("onedarkpro.helpers").get_colors("onedark_vivid")

local onedarkpro = {}

onedarkpro.normal = {
  a = { bg = colors.blue, fg = colors.bg },
  b = { bg = colors.fg_gutter, fg = colors.fg },
  c = { bg = colors.bg_statusline, fg = colors.fg },
}

onedarkpro.insert = {
  a = { bg = colors.green, fg = colors.bg },
  b = { bg = colors.fg_gutter, fg = colors.fg },
}

onedarkpro.command = {
  a = { bg = colors.blue, fg = colors.bg },
  b = { bg = colors.fg_gutter, fg = colors.fg },
}

onedarkpro.visual = {
  a = { bg = colors.yellow, fg = colors.bg },
  b = { bg = colors.fg_gutter, fg = colors.fg },
}

onedarkpro.replace = {
  a = { bg = colors.red, fg = colors.bg },
  b = { bg = colors.fg_gutter, fg = colors.fg },
}

onedarkpro.inactive = {
  a = { bg = colors.bg_statusline, fg = colors.blue },
  b = { bg = colors.bg_statusline, fg = colors.fg_gutter, gui = "bold" },
  c = { bg = colors.bg_statusline, fg = colors.fg_gutter },
}

return onedarkpro

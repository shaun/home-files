vim.g.mapleader = ","
vim.g.maplocalleader = ","
vim.g.snacks_animate = false

-- General
vim.opt.clipboard = ""
vim.opt.relativenumber = false
vim.opt.swapfile = false
vim.opt.confirm = true -- Enables a confirmation dialog to save unsaved work when quitting

-- Presentation/TUI-related
-- stylua: ignore start
vim.opt.termguicolors = true             -- Enable 24-bit RGB color
vim.opt.mouse = 'a'                      -- Enable mouse mode
vim.opt.showmode = false                 -- Hide the default mode text (e.g. -- INSERT -- below the statusline
vim.opt.signcolumn = 'yes'               -- Always show the sign column (aka the gutter
vim.opt.list = false                     -- Hide characters that represent tab, trailing spaces, and non-breaking spaces
vim.opt.completeopt = 'menuone,noselect'
vim.opt.wrap = true
vim.opt.breakindent = true
-- stylua: ignore end

-- Window splits
vim.opt.splitbelow = true
vim.opt.splitright = true
vim.opt.equalalways = true

-- Searching
-- stylua: ignore start
vim.opt.ignorecase = true -- Enable case-insensitive searching
vim.opt.smartcase = true  -- Override `ignorecase` if search pattern contains an uppercase character
vim.opt.hlsearch = true   -- Highlight search matches
-- stylua: ignore end

-- Tabs
vim.opt.smartindent = true
vim.opt.expandtab = true
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.softtabstop = 2

-- Lines
-- stylua: ignore start
vim.opt.number = true -- Show line numbers
vim.opt.cursorline = true  -- Highlight the line with the cursor
vim.opt.linebreak = true   -- Prevent line breaks in the middle of words
vim.opt.scrolloff = 10     -- Minimum number of lines to keep above/below the cursor
vim.opt.sidescrolloff = 10 -- Minimum number of lines to keep right/left of the cursor
-- stylua: ignore end

-- History
-- stylua: ignore start
vim.opt.undolevels = 500 -- Maximum number of changes that can be undone
vim.opt.history = 500    -- Maximum number of (: commands and (/) searches to keep
vim.opt.undofile = true  -- Enable persistent undo (writes undo history to a file
-- stylua: ignore end

-- Ctags
vim.opt.tags:prepend({ ".git/tags" })

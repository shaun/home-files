local function augroup(name)
  return vim.api.nvim_create_augroup(name, { clear = true })
end

-- Dynamic cursorline - show cursorline when not in insert mode
vim.api.nvim_create_autocmd({ "Insertleave", "WinEnter" }, {
  group = augroup("set_cursorline"),
  callback = function()
    vim.opt.cursorline = true
  end,
})

-- Dynamic cursorline - hide cursorline when in insert mode
vim.api.nvim_create_autocmd({ "InsertEnter", "WinLeave" }, {
  group = augroup("set_no_cursorline"),
  callback = function()
    vim.opt.cursorline = false
  end,
})

-- Highlight yanked text
vim.api.nvim_create_autocmd("TextYankPost", {
  group = augroup("highlight_text_on_yank"),
  callback = function()
    vim.highlight.on_yank({ higroup = "HlSearchNear", timeout = 300 })
  end,
})

vim.api.nvim_create_autocmd("TermOpen", {
  group = augroup("terminal_settings"),
  callback = function()
    vim.opt_local.number = false
    vim.opt_local.signcolumn = "no"

    vim.keymap.set("n", "<Esc>", function()
      if vim.bo.buftype == "terminal" then
        vim.cmd("bdelete!")
      end
    end, { remap = true, silent = true })

    vim.keymap.set("t", "<Esc>", "<C-\\><C-n>:bdelete!<CR>", { noremap = true, silent = true })
  end,
})

-- Set spellchecking and linewrapping for certain filetypes
vim.api.nvim_create_autocmd("FileType", {
  group = augroup("spellcheck_and_linewrap_settings"),
  pattern = { "text", "plaintex", "typst", "gitcommit", "markdown" },
  callback = function()
    vim.opt_local.spell = true
    vim.opt_local.wrap = true
  end,
})

-- Close certain filetypes with <q> or <esc> or <space>
vim.api.nvim_create_autocmd("FileType", {
  group = augroup("close_filetypes_with_keymapping"),
  pattern = {
    "checkhealth",
    "fugitiveblame",
    "gitmessengerpopup",
    "help",
    "lazy",
    "lspinfo",
    "man",
    "notify",
    "NvimTree",
    "qf",
    "query",
    "snacks_notif",
    "snacks_notif_history",
    "startuptime",
  },
  callback = function(event)
    vim.bo[event.buf].buflisted = false
    vim.keymap.set("n", "q", "<cmd>close<cr>", { buffer = event.buf, silent = true })
    vim.keymap.set("n", "<esc>", "<cmd>close<cr>", { buffer = event.buf, silent = true })
    vim.keymap.set("n", "<space>", "<cmd>close<cr>", { buffer = event.buf, silent = true })
  end,
})

-- Setup custom diagnostics icons that are displayed in the gutter
for type, icon in pairs(require("global-vars").icons.diagnostics) do
  local highlight = "DiagnosticSign" .. type

  vim.fn.sign_define(highlight, { text = icon, texthl = highlight, numhl = highlight })
end

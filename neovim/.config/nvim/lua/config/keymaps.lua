-------------------------------------------------
-- Delete certain LazyVim keymappings
-------------------------------------------------

vim.keymap.del("n", "<leader>.") -- Toggle scratch buffer
vim.keymap.del("n", "<leader>S") -- Select scratch buffer
vim.keymap.del("n", "<leader>n") -- Notification history
vim.keymap.del("n", "<leader>?") -- Buffer keymaps

-------------------------------------------------
-- General Vim improvements
-------------------------------------------------

vim.api.nvim_set_keymap(
  "n",
  "*",
  "*N",
  { noremap = true, silent = true, desc = "Don't move when searching for word under cursor" }
)

vim.api.nvim_set_keymap(
  "n",
  "J",
  "mmJ`m",
  { noremap = true, silent = true, desc = "Don't move the cursor when joining lines" }
)

vim.api.nvim_set_keymap(
  "v",
  "<Tab>",
  ">gv",
  { noremap = true, silent = true, desc = "Reselect selection after indenting" }
)

vim.api.nvim_set_keymap(
  "v",
  "<S-Tab>",
  "<gv",
  { noremap = true, silent = true, desc = "Reselect selection after outdenting" }
)

vim.api.nvim_set_keymap(
  "n",
  "k",
  "gk",
  { noremap = true, silent = true, desc = "Move to line above when words are wrapped to next line" }
)

vim.api.nvim_set_keymap(
  "n",
  "j",
  "gj",
  { noremap = true, silent = true, desc = "Move to line below when words are wrapped to next line" }
)

vim.api.nvim_set_keymap(
  "v",
  "<Leader>y",
  '"+y',
  { noremap = true, silent = true, desc = "Copy/yank to system clipboard" }
)

-- stylua: ignore
vim.api.nvim_set_keymap(
  "i",
  "jk",
  "<ESC>",
  { noremap = true, silent = true, desc = "`jk` to escape insert mode" }
)

-- stylua: ignore
vim.api.nvim_set_keymap(
  "i",
  "jl",
  "<ESC>",
  { noremap = true, silent = true, desc = "`jl` to escape insert mode" }
)

vim.api.nvim_set_keymap(
  "n",
  "<C-k>",
  ":write<CR>",
  { noremap = true, silent = true, desc = "Save file (in normal mode)" }
)

vim.api.nvim_set_keymap(
  "i",
  "<C-k>",
  "<ESC>:write<CR>",
  { noremap = true, silent = true, desc = "Save file (in insert mode)" }
)

vim.api.nvim_set_keymap(
  "v",
  "<C-k>",
  "<ESC>:write<CR>",
  { noremap = true, silent = true, desc = "Save file (in visual mode)" }
)

vim.api.nvim_set_keymap(
  "v",
  "<C-l>",
  "<ESC>:nohlsearch<CR>",
  { noremap = true, silent = true, desc = "Exit visual mode and clear the current search highlighting" }
)

-- stylua: ignore
vim.api.nvim_set_keymap(
  "n",
  "Y",
  "y$",
  { noremap = true, silent = true, desc = "Yank to the end of the line" }
)

vim.keymap.set(
  { "n", "x" },
  "y",
  "<Plug>(YankyYank)",
  { noremap = true, silent = true, desc = "Preserve cursor position after doing a yank" }
)

-------------------------------------------------
-- General shortcuts
-------------------------------------------------

vim.api.nvim_set_keymap(
  "n",
  "<Space>",
  ":call utilities#CloseAllTheThings()<CR>",
  { noremap = true, silent = true, desc = "Close all the things" }
)

-------------------------------------------------
-- Buffers
-------------------------------------------------

vim.keymap.set("n", ",,", function()
  Snacks.picker.buffers({
    preview = "false",
    layout = "bottom",
    finder = "buffers",
    format = "buffer",
    hidden = false,
    unloaded = true,
    current = true,
    sort_lastused = true,
    win = {
      input = {
        keys = {
          ["C-d"] = "bufdelete",
        },
      },
      list = { keys = { ["C-d"] = "Delete buffer" } },
    },
  })
end, {
  noremap = true,
  silent = true,
  desc = "Open buffer picker",
})

vim.api.nvim_set_keymap(
  "n",
  "tt",
  ":b#<CR>",
  { noremap = true, silent = true, desc = "Toggle between prev/current buffer" }
)

-------------------------------------------------
-- Finding things
-------------------------------------------------

vim.keymap.set("n", "<leader>ff", function()
  Snacks.picker.smart({
    multi = { "buffers", "recent", "files" },
    format = "file",
    matcher = {
      frecency = true, -- Frecency bonus
      cwd_bonus = true, -- CWD bonus
      sort_empty = true, -- Sort results when the search string is empty
    },
    transform = "unique_file",
  })
end, { noremap = true, silent = true, desc = "Find (f)iles" })

vim.keymap.set("n", "<leader>fh", function()
  Snacks.picker.help()
end, { noremap = true, silent = true, desc = "Find (h)elp" })

vim.keymap.set("n", "<leader>fH", function()
  Snacks.picker.help()
end, { noremap = true, silent = true, desc = "Find (H)ighlights" })

vim.keymap.set("n", "<leader>fi", function()
  Snacks.picker.icons()
end, { noremap = true, silent = true, desc = "Find (i)cons" })

vim.keymap.set({ "n", "v" }, "*r", function()
  Snacks.picker.grep_word()
end, { noremap = true, silent = true, desc = "Grep search the word under cursor" })

vim.api.nvim_set_keymap(
  "n",
  "gt",
  "g<C-]>",
  { noremap = true, silent = true, desc = "Go to the (tag) definition of keyword/tag under cursor" }
)

vim.keymap.set("n", "<Leader>rg", function()
  Snacks.picker.grep({})
end, { noremap = true, silent = true, desc = 'Run/start a "live" grep search' })

-------------------------------------------------
-- Git shortcuts
-------------------------------------------------

-- stylua: ignore
vim.api.nvim_set_keymap(
  "n",
  "<Leader>gb",
  ":Git blame<CR>",
  { noremap = true, silent = true, desc = "Git blame" }
)

vim.api.nvim_set_keymap(
  "n",
  "<Leader>gd",
  ":Gitsigns preview_hunk<CR>",
  { noremap = true, silent = true, desc = "Git diff" }
)

-- stylua: ignore
vim.api.nvim_set_keymap(
  "n",
  "<Leader>gll",
  ":GV<CR>",
  { noremap = true, silent = true, desc = "Git log" }
)

vim.api.nvim_set_keymap(
  "n",
  "<Leader>glf",
  ":GV!<CR>",
  { noremap = true, silent = true, desc = "Git log for current file" }
)

vim.api.nvim_set_keymap(
  "n",
  "<Leader>gln",
  ":GitMessenger<CR>",
  { noremap = true, silent = true, desc = "Git log for current line" }
)

vim.api.nvim_set_keymap(
  "n",
  "]g",
  ":Gitsigns next_hunk<CR>",
  { noremap = true, silent = true, desc = "Go to next git hunk" }
)

vim.api.nvim_set_keymap(
  "n",
  "[g",
  ":Gitsigns prev_hunk<CR>",
  { noremap = true, silent = true, desc = "Go to prev git hunk" }
)

vim.api.nvim_set_keymap(
  "n",
  "<Leader>gu",
  ":Gitsigns reset_hunk<CR>",
  { noremap = true, silent = true, desc = "Reset current hunk" }
)

-------------------------------------------------
-- Testing shortcuts
-------------------------------------------------

-- stylua: ignore
vim.api.nvim_set_keymap(
  "n",
  "<CR>",
  ":TestNearest<CR>",
  { noremap = true, silent = true, desc = "Run test nearest to the cursor" }
)

-- stylua: ignore
vim.api.nvim_set_keymap(
  "n",
  "<Leader>rr",
  ":TestFile<CR>",
  { noremap = true, silent = true, desc = "Run tests for the current file" }
)

-- stylua: ignore
vim.api.nvim_set_keymap(
  "n",
  "<Leader>rl",
  ":TestL<CR>",
  { noremap = true, silent = true, desc = "Run last test" }
)

-------------------------------------------------
-- Toggle Shortcuts
-------------------------------------------------

vim.api.nvim_set_keymap(
  "n",
  "<Leader>tb",
  ":Telescope buffers<CR>",
  { noremap = true, silent = true, desc = "Toggle buffers list" }
)

vim.api.nvim_set_keymap(
  "n",
  "<Leader>td",
  ":Trouble diagnostics toggle filter.buf=0<CR>",
  { noremap = true, silent = true, desc = "Toggle diagnostics" }
)

vim.api.nvim_set_keymap(
  "n",
  "<Leader>tD",
  ":lua Snacks.dashboard.open()<CR>",
  { noremap = true, silent = true, desc = "Toggle dashboard" }
)

-- stylua: ignore
vim.api.nvim_set_keymap(
  "n",
  "<Leader>tg",
  ":Neogit<CR>",
  { noremap = true, silent = true, desc = "Toggle Neogit" }
)

-- stylua: ignore
vim.api.nvim_set_keymap(
  "n",
  "<Leader>tl",
  ":Lazy<CR>",
  { noremap = true, silent = true, desc = "Toggle Lazy" }
)

-- stylua: ignore
vim.keymap.set(
  "n",
  "<Leader>tln",
  function() LazyVim.news.lazyvim() end,
  { noremap = true, silent = true, desc = "Toggle LazyVim News" }
)

-- stylua: ignore
vim.api.nvim_set_keymap(
  "n",
  "<Leader>tm",
  ":Mason<CR>",
  { noremap = true, silent = true, desc = "Toggle Mason" }
)

vim.api.nvim_set_keymap(
  "n",
  "<Leader>tn",
  ":NvimTreeToggle<CR>",
  { noremap = true, silent = true, desc = "Toggle file explorer" }
)

-- stylua: ignore
vim.keymap.set("n",
  "<Leader>tN",
  function() Snacks.notifier.show_history() end,
  { noremap = true, silent = true, desc = "Toggle notifications" }
)

vim.api.nvim_set_keymap(
  "n",
  "<Leader>tq",
  ":call ToggleQuickfixList()<CR>",
  { noremap = true, silent = true, desc = "Toggle quickfix list" }
)

vim.api.nvim_set_keymap(
  "n",
  "<Leader>ts",
  ":Telescope persisted<CR>",
  { noremap = true, silent = true, desc = "Toggle session manager" }
)

vim.api.nvim_set_keymap(
  "n",
  "<Leader>tu",
  ":UndotreeToggle<CR>",
  { noremap = true, silent = true, desc = "Toggle undo tree" }
)

vim.api.nvim_set_keymap(
  "n",
  "<Leader>ty",
  ":Telescope yank_history<CR>",
  { noremap = true, silent = true, desc = "Toggle yank history" }
)

-------------------------------------------------
-- Snacks toggle shortcuts
-------------------------------------------------

Snacks.toggle.option("spell", { name = "Spelling" }):map("<leader>tS")
Snacks.toggle.animate():map("<leader>ua")
Snacks.toggle.dim():map("<leader>ud")

-------------------------------------------------
-- Whichkey configuration
-------------------------------------------------

require("which-key").add({
  { "<leader>c", group = "(c)ode (LSP)" },
  { "<leader>d", group = "(d)ebugger" },
  { "<leader>f", group = "(f)ind files" },
  { "<leader>fl", group = "(l)anguage files" },
  { "<leader>g", group = "(g)it" },
  { "<leader>r", group = "(r)un" },
  { "<leader>s", group = "(s)earch" },
  { "<leader>t", group = "(t)oggle" },
  { "<leader>u", group = "(u)i" },
  { "<leader>w", group = "(w)indows" },
  { "<leader>,", hidden = true }, -- Hide keymapping to show buffers
  { "<leader>:", hidden = true }, -- Hide keymapping to show command history
})

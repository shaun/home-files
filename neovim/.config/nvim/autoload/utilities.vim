function! utilities#CloseAllTheThings()
  :cclose
  :pclose
  :NvimTreeClose
  :GitMessengerClose
  :UndotreeHide
  :Trouble diagnostics close
  :lua Snacks.notifier.hide()

  for wnr in range(1, winnr('$'))
    if !empty(getwinvar(wnr, 'fugitive_leave'))
      execute winbufnr(wnr).'bdelete'
    endif
  endfor
endfunction

let s:onedark_colors = onedark#GetColors()

let s:black = [ '#282c34', 236 ]
let s:dark_gray = [ '#363c47', 237 ]
let s:light_gray = [ '#5d677a', 243 ]
let s:white = [ '#dcdfe4', 255 ]

let s:red = [ s:onedark_colors.red.gui, s:onedark_colors.red.cterm ]
let s:green = [ s:onedark_colors.green.gui, s:onedark_colors.green.cterm ]
let s:yellow = [ '#fad07a', 180 ]
let s:blue = [ s:onedark_colors.blue.gui, s:onedark_colors.blue.cterm ]
let s:purple = [ s:onedark_colors.purple.gui, s:onedark_colors.purple.cterm ]
let s:cyan = [ s:onedark_colors.cyan.gui, s:onedark_colors.cyan.cterm ]

let s:p = {'normal': {}, 'inactive': {}, 'insert': {}, 'replace': {}, 'visual': {}, 'tabline': {}}

let s:p.normal.left = [ [ s:black, s:white ], [ s:white, s:light_gray ] ]
let s:p.normal.middle = [ [ s:white, s:dark_gray ] ]
let s:p.normal.right = [ [ s:black, s:white ], [ s:white, s:light_gray ] ]

let s:p.normal.error = [ [ s:black, s:red ] ]
let s:p.normal.warning = [ [ s:black, s:yellow ] ]

let s:p.inactive.left =  [ [ s:white, s:light_gray ], [ s:white, s:light_gray ] ]
let s:p.inactive.middle = [ [ s:white, s:dark_gray ] ]
let s:p.inactive.right = [ [ s:black, s:white ], [ s:white, s:light_gray ] ]

let s:p.insert.left = [ [ s:black, s:purple ], [ s:white, s:light_gray ] ]
let s:p.replace.left = [ [ s:black, s:red ], [ s:white, s:light_gray ] ]
let s:p.visual.left = [ [ s:black, s:yellow ], [ s:white, s:light_gray ] ]

let s:p.tabline.left = [ [ s:light_gray, s:dark_gray] ]
let s:p.tabline.tabsel = [ [ s:white, s:light_gray ] ]
let s:p.tabline.middle = [ [ s:light_gray, s:dark_gray] ]
let s:p.tabline.right = [ [ s:black, s:white ] ]

let g:lightline#colorscheme#onedarkcustom#palette = lightline#colorscheme#flatten(s:p)

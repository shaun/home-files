function! git#CompleteUsernames(findstart, base)
  if a:findstart
    " locate the start of the word
    let line = getline('.')
    let start = col('.') - 1
    while start > 0 && line[start - 1] =~ '\a'
      let start -= 1
    endwhile
    return start
  else
    let result = []
    for pair in readfile(expand('~/.vim/.pair-programmers/.usernames'))
      if pair =~ '^' . a:base
        call add(result, pair)
      endif
    endfor
    return result
  endif
endfunction

function! git#CompleteCoAuthors(findstart, base)
  if a:findstart
    " locate the start of the word
    let line = getline('.')
    let start = col('.') - 1
    while start > 0 && line[start - 1] =~ '\a'
      let start -= 1
    endwhile
    return start
  else
    let result = []
    for co_author in readfile(expand('~/.vim/.pair-programmers/.co-authors'))
      if co_author =~ '^' . a:base
        call add(result, co_author)
      endif
    endfor
    return result
  endif
endfunction

function! git#InsertEmoji()
  call git#InsertTextAndTriggerCompleteFunc({ 'completefunc': 'emoji#complete' })
  call feedkeys("\<C-H>\<C-H>\<C-H>")
endfunction

function! git#InsertTextAndTriggerCompleteFunc(settings)
 let &l:completefunc=a:settings.completefunc

  execute "startinsert!"

  if exists('a:settings.text')
    call feedkeys(a:settings.text)
  endif

  call feedkeys("\<C-X>\<C-U>")
endfunction

function! git#GitLogForFile()
  setlocal foldlevelstart=1
  GV!
endfunction

function! git#GitLog()
  setlocal foldlevelstart=1
  GV
endfunction

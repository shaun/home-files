setlocal spell
setlocal complete+=kspell

if executable('par')
  setlocal formatprg=par
endif

inoremap <silent><buffer><c-l><c-c> <esc>:call git#InsertTextAndTriggerCompleteFunc({ 'text': 'Co-authored-by: ', 'completefunc': 'git#CompleteCoAuthors' })<cr>
nnoremap <silent><buffer><c-l><c-c>      :call git#InsertTextAndTriggerCompleteFunc({ 'text': 'Co-authored-by: ', 'completefunc': 'git#CompleteCoAuthors' })<cr>

inoremap <silent><buffer><c-l><c-e> <esc>:call git#InsertEmoji()<cr>
nnoremap <silent><buffer><c-l><c-e>      :call git#InsertEmoji()<cr>

inoremap <silent><buffer><c-l><c-f> <esc>:call utilities#FormatParagraph()<cr>
nnoremap <silent><buffer><c-l><c-f>      :call utilities#FormatParagraph()<cr>

inoremap <silent><buffer><c-l><c-p> <esc>:call git#InsertTextAndTriggerCompleteFunc({ 'text': ':pear: w/ ', 'completefunc': 'git#CompleteUsernames' })<cr>
nnoremap <silent><buffer><c-l><c-p>      :call git#InsertTextAndTriggerCompleteFunc({ 'text': ':pear: w/ ', 'completefunc': 'git#CompleteUsernames' })<cr>

inoremap <silent><buffer><c-l><c-u> <esc>:call git#InsertTextAndTriggerCompleteFunc({ 'completefunc': 'git#CompleteUsernames' })<cr>
nnoremap <silent><buffer><c-l><c-u>      :call git#InsertTextAndTriggerCompleteFunc({ 'completefunc': 'git#CompleteUsernames' })<cr>

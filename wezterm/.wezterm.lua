local wezterm = require("wezterm")
local config = {}

config.color_scheme = "OneDarkPro Vivid"

config.font = wezterm.font_with_fallback({
	{
		family = "MonoLisa",
		harfbuzz_features = {
			"ss02", -- enable script variant
			"ss07", -- use alternant curly braces
			"ss08", -- use alternant parentheses
			"ss15", -- use sans-serif "i"
		},
	},

	"JetBrains Mono",
	"DejaVuSansMono Nerd Font Mono",
	"Noto Sans Mono",
	"Droid Sans Fallback",
})

config.font_size = 13.0

config.warn_about_missing_glyphs = false

config.hide_tab_bar_if_only_one_tab = true
config.window_padding = {
	top = 0,
	bottom = 0,
	left = 4,
	right = 4,
}

return config

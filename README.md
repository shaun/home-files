# :package: Home Files :package:

I use Stow to manage my home files (mainly dotfiles) that I want keep in sync between my computers.

Each top level directory (e.g., `neovim`, `tmux`, `git`) is a [Stow](https://www.gnu.org/software/stow/) "package" that contains configuration settings, tools, etc.

The `stow` command will install and symlink (or remove) the package contents.

#### Notes/Examples

I don't manually run the `stow` command nowadays because my home file are kept in sync with my custom Ansible tasks via [my-machine-manager](https://framagit.org/shaun/my-ma-ma). The notes below are what I used for reference before I started using Ansible.

For example, from within the root stow directory, there is a `neovim` directory/package, to install Neovim configuration files into the home directory, run:

```
stow --target=$HOME neovim
```

The default target directory (where things will get installed/stowed) is the parent directory of the stow directory, which in my case is my home directory; since I clone this repo into `~/.home-files`.

I set the default stow directory (where the packages live) via the environment variable `STOW_DIR`. With this setup I can run stow commands from any directory to install packages into my home folder with something like:

```
stow neovim
```

To restow a package:
```
stow --restow neovim
```

[:books: Stow Documentation](https://www.gnu.org/software/stow/manual/stow.html)
